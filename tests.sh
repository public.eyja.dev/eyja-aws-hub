#!/bin/sh

docker run -d --name nats-test -p 30004:4222 nats:2.7.0-alpine3.15

sleep 5

python -W ignore::DeprecationWarning -m unittest

docker stop nats-test && docker rm nats-test